package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class LeadPage extends ProjectMethods{

	
	public LeadPage() {
		PageFactory.initElements(driver, this);
	} 
	@FindBy(linkText="Merge Leads")WebElement MergeLeads;
	public MergeLead clickMergeLeads() {
	    click(MergeLeads);  
	   return new MergeLead();
	    
	}
	
}







