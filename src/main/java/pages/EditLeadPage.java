package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class EditLeadPage extends ProjectMethods{


	public  EditLeadPage() {
		PageFactory.initElements(driver, this);
	} 
	@FindBy(xpath="(//input[@name='firstName'])[3]")
	WebElement editFname;
		public EditLeadPage EditFname() {
			type(editFname, "data");
			return this;
		}
	 @FindBy(xpath="(//input[@name='submitButton'])[1]")
	 WebElement Update;
	 public ViewLead Update() {
	 click(Update);
	 return  new ViewLead();
	 }
	 }
	 
	 
	 

	
