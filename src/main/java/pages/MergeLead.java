package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeLead extends ProjectMethods{


	public MergeLead() {
		PageFactory.initElements(driver, this);
	} 
	@FindBy(xpath="(//div[@class='subSectionBlock']//img)[1]")
	WebElement clickFromLead;
	public MergeLead fromLead() {
		click(clickFromLead);
		switchToWindow(1);
		return this;
		
	}
	@FindBy(xpath="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]//a")
	  WebElement img1;
	public MergeLead getText(String data) {
		leadID=img1.getText();
		System.out.println(leadID);
		return this;
		}
	
	 @FindBy(xpath="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]//a")
	  WebElement eleimg1;
	public MergeLead lookUpLeads() {
		click(eleimg1);
		switchToWindow(0);
		return this;
		}
	
	 @FindBy(xpath="(//div[@class='subSectionBlock']//img)[2]")
	  WebElement img2;
	public MergeLead formLead1() {
		click(img2);
		switchToWindow(1);
		return this;
		}
	

	 @FindBy(xpath="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[2]//a")
	  WebElement eleimg2;
	public MergeLead lookUpLeads1() {
		click(eleimg2);
		switchToWindow(0);
		return this;
		}
	
	@FindBy(xpath="//a[@class='buttonDangerous']")
	WebElement button;
	public MergeLead mergeButton() {
		click(button);
		return this;
		
	}
	
	public ViewLead handleAlert() {
		acceptAlert();
		return new ViewLead();
	}
}
