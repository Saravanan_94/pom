package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLead extends ProjectMethods{


	public FindLead() {
		PageFactory.initElements(driver, this);
	} 
		
	@FindBy(xpath="//input[@name='id']")
	WebElement findLeadButton;
 public FindLead findLeadInput() {
	 type(findLeadButton,leadID);
	 return this;
 }

  @FindBy(xpath="//button[text()='Find Leads']")
  WebElement findLeadsButton;
 public FindLead findLeads()  {
	
	click(findLeadsButton);
	try {
		Thread.sleep(2000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} 
	return this;
 }
 
 @FindBy(xpath="//div[@class='x-paging-info']")
 WebElement text;
 
 public FindLead validateText(String data) {
	 verifyExactText(text, data);
	return this; 
	 
 }
 @FindBy(xpath="//a[text()='Find Leads']")
 WebElement findlead1;
 public FindLead findlead1() {
	 click(findlead1);
	 return this;
 
 }
 
 public FindLead FindFirstName(String data) {
		WebElement eleffn = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(eleffn, data);
		return this;
	}

		public FindLead ClickFirstName() {
		WebElement Firstname = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
		click(Firstname);
		return this;
	}
		public ViewLead FindLeadButton() {
			WebElement findleadbutton = locateElement("xpath", "(//button[@class='x-btn-text'])[7]");
			click(findleadbutton);
			return  new ViewLead();
		} 

}


