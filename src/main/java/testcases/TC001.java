package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC001 extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName = "TC001";
		testDescription = "Login into leaftaps";
		testNodes = "Leads";
		authors ="panneer";
		category="smoke";
		dataSheetName = "TC001";
	}

	@Test(dataProvider="fetchData")
	public void logInLogOut(String uname, String pwd) {
		new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin()
		.clickcrmsfa()
		.Leads()
		.clickMergeLeads()
		.fromLead()
		.getText(leadID)
		.lookUpLeads()
		.formLead1()
		.lookUpLeads1()
		.mergeButton()
		.handleAlert()
		.findLead()
		.findLeadInput()
		.findLeads();
	}
}









