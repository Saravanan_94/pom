package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002 extends ProjectMethods
{
	@BeforeTest
	public void setData() {
		testCaseName = "TC002";
		testDescription = "Login into leaftaps";
		testNodes = "Leads";
		authors ="Gayatri";
		category="smoke";
		dataSheetName = "TC002";
	}

	@Test(dataProvider="fetchData")
	public void logInLogOut(String uname, String pwd) {
		new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin()
		.clickcrmsfa()
		.Leads()
		.findlead1()
		.ClickFirstName()
		.FindLeadButton()
		.click(clickEditbutton);
}
}

