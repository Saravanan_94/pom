Feature: Login into Leaftaps
Background:
Given Launch the browser
And Load the URL
And Maximize the browser
And Set the timeouts

Scenario Outline: Login  positive
And Enter the username as <username>
And Enter the Password as <pass>
#When Verify the welcome Message 
Then Click CRM/SFA
Then Click Create Lead
Then Enter the company name
Then Enter the first name
And Enter the Last name
When Click on the Create Lead

Examples: 
		|username|pass|
		|DemoSalesManager|crmsfa|
		|DemoCSR|crmsfa|
		
Scenario Outline: Login  negative
And Enter the username as <username>
And Enter the Password as <pass>
When Click the Login button
#When Verify the welcome Message 
Then Click CRM/SFA
Then Click Create Lead
Then Enter the company name
Then Enter the first name
And Enter the Last name
When Click on the Create Lead

Examples: 
		|username|pass|
		|DemoSalesManager|crmsfa|
		|DemoCSR|crmsfa|