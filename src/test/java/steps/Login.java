package steps;

import java.util.concurrent.TimeUnit;

import org.junit.runner.RunWith;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.CucumberOptions;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.junit.Cucumber;
@RunWith(Cucumber.class)
@CucumberOptions
public class Login {
	ChromeDriver driver;
	@Given("Launch the browser")
	public void launchTheBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver=new ChromeDriver();
		

	}
	@Given("Load the URL")
	public void load_the_URL() {
		driver.get("http://leaftaps.com/opentaps/control/main");

	}

	@Given("Maximize the browser")
	public void maximize_the_browser() {
		driver.manage().window().maximize();


	}

	@Given("Set the timeouts")
	public void set_the_timeouts() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);


	}

	@Given("Enter the username as(.*)")
	public void enter_the_username_Demo_Sales_Manager(String username) {
		driver.findElementById("username").sendKeys("DemoSalesManager");


	}
	/*@Given("Enter the username as(.*)")
	public void enter_the_username_Demo_CSR(String username) {
		driver.findElementById("username").sendKeys("DemoSalesManager");
		}
*/

	

	@Given("Enter the Password as(.*)")
	public void enter_the_Password_crm_sfa(String pass) {
		driver.findElementById("password").sendKeys("crmsfa");


	}
	

	@When("Click the Login button")
	public void click_the_Login_button() {
		driver.findElementByClassName("decorativeSubmit").click();


	}

	@Then("Verify the welcome Message")
	public void verify_the_welcome_Message() {
		String text = driver.findElementByTagName("h2").getText();
		System.out.println(text);
		if(text.contains(text)) {
			System.out.println("pass");
		} else {
			System.out.println("fail");
		}
		driver.close();
	}

	/*@Then("Verify the Welcome message as B2B")
	public void verifyTheWelcomeMessageAsB2B() {
		String text = driver.findElementByTagName("h2").getText();
		System.out.println(text);
		if(text.contains("B2B")) {
			System.out.println("pass");
		} else {
			System.out.println("fail");
		}
	}*/

	@Then("Click CRM\\/SFA")
	public void clickCRMSFA() {
		driver.findElementByLinkText("CRM/SFA").click();



	}

	@Then("Click Create Lead")
	public void clickCreateLead() {
		driver.findElementByLinkText("Create Lead").click();
    

	}

	@Then("Enter the company name")
	public void enterTheCompanyName() {
		driver.findElementById("createLeadForm_companyName").sendKeys("TL");
		
	}

	@Then("Enter the first name")
	public void enterTheFirstName() {
		driver.findElementById("createLeadForm_firstName").sendKeys("Test");
		
	      
	    
	}

	@Then("Enter the Last name")
	public void enterTheLastName() {
		driver.findElementById("createLeadForm_lastName").sendKeys("leaf");
	    
	}

	@When("Click on the Create Lead")
	public void clickOnTheCreateLead() {
	    
		driver.findElementByName("submitButton").click();  
	}

	
	

}
